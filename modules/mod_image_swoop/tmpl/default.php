<?php
/**
 * Camera Slideshow for Joomla! Module
 *
 * @author    TemplateMonster http://www.templatemonster.com
 * @copyright Copyright (C) 2012 - 2013 Jetimpex, Inc.
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 
 * Parts of this software are based on Camera Slideshow By Manuel Masia: http://www.pixedelic.com/plugins/camera/ & Articles Newsflash standard module
 * 
 */

defined('_JEXEC') or die;

$app 	  = JFactory::getApplication();	
$template = $app->getTemplate();

//Add script
$document->addScript('modules/mod_image_swoop/js/camera.min.js');

//Add stylesheet
$document->addStyleSheet('modules/mod_image_swoop/css/camera.css');

 ?>
<div id="camera-slideshow_<?php echo $module->id; ?>" class="<?php echo $moduleclass_sfx; ?> camera_wrap pattern_<?php echo $params->get('pattern'); ?>">
<?php
	// Item URL
	if($params->get('item_url')){
		$itemURLs = explode(';', $params->get('item_url'));
	}	

	// Item width
	$item_width = floor(100 / count($list));


	$i=0;	
	foreach ($list as $item) :		
		require JModuleHelper::getLayoutPath('mod_image_swoop', '_item');
		$i++;
	endforeach;
?>
</div>
<?php $document->addScriptdeclaration('jQuery(function($){$("#camera-slideshow_'.$module->id.'").camera({alignment:"'.$params->get('alignment').'",autoAdvance:'.$params->get("autoAdvance").',mobileAutoAdvance:'.$params->get("mobileAutoAdvance").',barDirection:"'.$params->get('barDirection').'",barPosition:"'.$params->get('barPosition').'",cols:'.$params->get("cols").',easing:"'.$params->get('easing').'",mobileEasing:"'.$params->get('mobileEasing').'",fx:"'.$params->get('fx').'",mobileFx:"'.$params->get('mobileFx').'",gridDifference:'.$params->get("gridDifference").',height:"'.$params->get('height').'",imagePath:"'.JURI::base(true).'/templates/'.$template.'/images/",hover:'.$params->get("hover").',loader:"'.$params->get('loader').'",loaderColor:"'.$params->get('loaderColor').'",loaderBgColor:"'.$params->get('loaderBgColor').'",loaderOpacity:'.$params->get("loaderOpacity").',loaderPadding:'.$params->get("loaderPadding").',loaderStroke:'.$params->get("loaderStroke").',minHeight:"'.$params->get('minHeight').'",navigation:'.$params->get("navigation").',navigationHover:'.$params->get("navigationHover").',mobileNavHover:'.$params->get("mobileNavHover").',opacityOnGrid:'.$params->get("opacityOnGrid").',overlayer:'.$params->get("overlayer").',pagination:'.$params->get("pagination").',playPause:'.$params->get("playPause").',pauseOnClick:'.$params->get("pauseOnClick").',pieDiameter:'.$params->get("pieDiameter").',piePosition:"'.$params->get('piePosition').'",portrait:'.$params->get("portrait").',rows:'.$params->get("rows").',slicedCols:'.$params->get("slicedCols").',slicedRows:'.$params->get("slicedRows").',thumbnails:'.$params->get("thumbnails").',time:'.$params->get("time").',transPeriod:'.$params->get("transperiod").'})});'); ?>