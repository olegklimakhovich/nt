<?php
/**
 * @package Module TM Ajax Contact Form for Joomla! 3.x
 * @version 2.0.0: mod_tm_ajax_contact_form.php
 * @author TemplateMonster http://www.templatemonster.com
 * @copyright Copyright (C) 2012 - 2014 Jetimpex, Inc.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2
**/
defined('_JEXEC') or die;
class modTmAjaxContactFormHelper{
	public static function recaptchaAjax(){
		JPluginHelper::importPlugin('captcha', 'recaptcha');
		$dispatcher = JEventDispatcher::getInstance();
		$res = $dispatcher->trigger('onCheckAnswer');
		if(!$res[0]){
			$result = 'e';
		  } else {
		    $result = "s";
		}
		return $result;
	}
	public static function getAjax(){
		JFactory::getLanguage()->load('com_contact');
		$input 		 = JFactory::getApplication()->input;
		$mail 		 = JFactory::getMailer();
		$inputs 	 = $input->get('data', array(), 'ARRAY');
		$formcontent = '';
		$key 		 = 0;
		foreach ($inputs as $name => $value){
			if(strpos($name, 'captcha') !== false) continue;
			if($name == 'module_id'){
				$db 			= JFactory::getDbo();
				$query 			= $db->getQuery(true);
				$query->select($db->quoteName('params'));
				$query->from($db->quoteName('#__modules'));
				$query->where($db->quoteName('id').' = '.$db->quote($value));
				$db->setQuery($query);
				$params 		= $db->loadResult();
				$params 		= json_decode($params);
				$failed 		= $params->failure_notify;
				$recipient 		= $params->admin_email;
				$cc_email 		= $params->cc_email;
				$bcc_email 		= $params->bcc_email;
				$fields_list	= json_decode($params->fields_list);
				$labels 		= $fields_list->label;
			}
			else{
				if($name == 'email'){
					$email = $value;
				}

                $subject = $params->emailSubject;

				$label = isset($labels[$key]) ? $labels[$key] : $name;
				$formcontent .= "<p><strong>".$label.":</strong> ".nl2br($value)."</p>";
				$key++;
			}

		}

        if(!empty($_FILES)){
            $files = [];

            foreach ($_FILES['files'] as $key=>$value){
                foreach ($value as $item => $attr){
                    $files[$item][$key] = $attr;
                }
            }
            $uploadedFiles = [];
            foreach ($files as $file){
                if($file['error']==0 && ($file['type']=="application/vnd.oasis.opendocument.text" || $file['type']=="application/pdf" || $file['type']=="application/msword" || $file['type']=="application/vnd.openxmlformats-officedocument.wordprocessingml.document")){
                    //имя файла
                    $fileName = "anketi/".uniqid()."_".$file['name'];
                    $filepath = JPATH_ROOT.'/'.$fileName;
                    $fileUrl = JUri::root().$fileName;
                    move_uploaded_file($file['tmp_name'], $filepath);
                    $uploadedFiles[$file['name']] = $fileUrl;
                }
            }
        }
		if(!empty($uploadedFiles)){
		    foreach ($uploadedFiles as $name => $fileUrl){
		        //echo $fileUrl.'хуйхуй';
                $formcontent .= "<p><strong>Файл:</strong> <a href = \"$fileUrl\">$name</a></p>";
            }
        }
		if(isset($recipient)){
			$sender = array();
			if(isset($email)) $mail->addReplyTo($email);
			$config = JFactory::getConfig();
			$global_sender = array(
			    $config->get('mailfrom'),
			    $config->get('fromname')
			);
			$mail->setSender($global_sender);
			$mail->addRecipient($recipient);
			if(!empty($cc_email)) {
                $ccArr = explode(',', $cc_email);
                array_map(function(&$value) use ($mail) {
                    $value = trim($value);
                    $mail->addCc($value);
                }, $ccArr);

            };
			if(isset($bcc_email)) $mail->addBCC($bcc_email);
			if(isset($subject)) $mail->setSubject($subject);
			$mail->isHTML(true);
			$mail->Encoding = 'base64';
			$mail->setBody($formcontent);
			$send = $mail->Send();
			if (isset($email)){
				$mail 			= JFactory::getMailer();
				$formcontent    = '<p>'.JText::sprintf('COM_CONTACT_COPYTEXT_OF', $config->get('fromname'), $_SERVER['HTTP_HOST']).'</p>'.$formcontent;
				if(isset($subject)){
					$copysubject = JText::sprintf('COM_CONTACT_COPYSUBJECT_OF', $subject);
					$mail->setSubject($copysubject);
				}
				$mail->addReplyTo($email);
				$mail->addRecipient($email);
				$mail->setSender($global_sender);
				$mail->isHTML(true);
				$mail->Encoding = 'base64';
				$mail->setBody($formcontent);
				$send = $mail->Send();
			}
			if ($send !== true){
			   	return '<span>'.$send->__toString().'</span>';
			} else {
			  	return "s";
			}
		}
		else{
			return "e";
		}
	}
}