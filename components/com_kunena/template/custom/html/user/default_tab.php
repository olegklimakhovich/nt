<?php
/**
 * Kunena Component
 * @package Kunena.Template.Blue_Eagle
 * @subpackage User
 *
 * @copyright (C) 2008 - 2013 Kunena Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link http://www.kunena.org
 **/
defined ( '_JEXEC' ) or die ();

JHtml::_('behavior.calendar');
JHtml::_('behavior.tooltip');
?>
<div id="kprofile-rightcoltop">
	<div class="kprofile-rightcol2">
<?php
	$this->displayTemplateFile('user', 'default', 'social');
?>
	</div>
	<div class="kprofile-rightcol1">
		<ul>
			<li><span class="kicon-profile kicon-profile-location"></span><strong><?php echo JText::_('COM_KUNENA_MYPROFILE_LOCATION'); ?>:</strong> <?php echo $this->locationlink; ?></li>
			<!--  The gender determines the suffix on the span class- gender-male & gender-female  -->
			<li class="bd"><span class="kicon-profile kicon-profile-birthdate"></span><strong><?php echo JText::_('COM_KUNENA_MYPROFILE_BIRTHDATE'); ?>:</strong> <span title="<?php echo KunenaDate::getInstance($this->profile->birthdate)->toKunena('ago', 'GMT'); ?>"><?php echo KunenaDate::getInstance($this->profile->birthdate)->toKunena('date', 'GMT'); ?></span>
			<!--  <a href="#" title=""><span class="bday-remind"></span></a> -->
			</li>
		</ul>
	</div>
</div>

<div class="clrline"></div>
<div id="kprofile-rightcolbot">
	<div class="kprofile-rightcol<?php if ($this->signatureHtml) : ?>2<?php endif ?>">
		<?php if ($this->email || !empty($this->profile->websiteurl)): ?>
			<ul>
				<?php if ($this->email): ?>
					<li><span class="kicon-profile kicon-profile-email"></span><?php echo $this->email; ?></li>
				<?php endif; ?>
				<?php if (!empty($this->profile->websiteurl)): ?>
					<?php // FIXME: we need a better way to add http/https ?>
					<li><span class="kicon-profile kicon-profile-website"></span><a href="<?php echo $this->escape($this->websiteurl); ?>" target="_blank"><?php echo KunenaHtmlParser::parseText(trim($this->profile->websitename) ? $this->profile->websitename : $this->websiteurl); ?></a></li>
				<?php endif; ?>
			</ul>
		<?php endif;?>
	</div>
	<?php if ($this->signatureHtml) : ?>
	<div class="kprofile-rightcol1">
		<h4><?php echo JText::_('COM_KUNENA_MYPROFILE_SIGNATURE'); ?></h4>
		<div class="kmsgsignature"><div><?php echo $this->signatureHtml ?></div></div>
	</div>
	<?php endif ?>

</div>

<div class="clrline"></div>

<div id="kprofile-tabs">
	<ul class="nav nav-tabs">
		<?php if($this->showUserPosts){ ?>
		<li class="active"><a data-toggle="tab" href="#tab-1"><?php echo JText::_('COM_KUNENA_USERPOSTS'); ?></a></li>
		<?php }
		if ($this->showSubscriptions){ ?>
		<li><a data-toggle="tab" href="#tab-2"><?php echo JText::_('COM_KUNENA_SUBSCRIPTIONS'); ?></a></li>
		<?php }
		if ($this->showFavorites){ ?>
		<li><a data-toggle="tab" href="#tab-3"><?php echo JText::_('COM_KUNENA_FAVORITES'); ?></a></li>
		<?php }
		if($this->showThankyou){ ?>
		<li><a data-toggle="tab" href="#tab-4"><?php echo JText::_('COM_KUNENA_THANK_YOU'); ?></a></li>
		<?php }
		if ($this->showUnapprovedPosts){ ?>
		<li><a data-toggle="tab" href="#tab-5"><?php echo JText::_('COM_KUNENA_MESSAGE_ADMINISTRATION'); ?></a></li>
		<?php }
		if ($this->showAttachments){ ?>
		<li><a data-toggle="tab" href="#tab-6"><?php echo JText::_('COM_KUNENA_MANAGE_ATTACHMENTS'); ?></a></li>
		<?php }
		if ($this->showBanManager){ ?>
		<li><a data-toggle="tab" href="#tab-7"><?php echo JText::_('COM_KUNENA_BAN_BANMANAGER'); ?></a></li>
		<?php }
		if ($this->showBanHistory){ ?>
		<li><a data-toggle="tab" href="#tab-8"><?php echo JText::_('COM_KUNENA_BAN_BANHISTORY'); ?></a></li>
		<?php }
		if ($this->showBanUser){ ?>
		<li><a data-toggle="tab" href="#tab-9"><?php echo $this->banInfo->id ? JText::_('COM_KUNENA_BAN_EDIT') : JText::_('COM_KUNENA_BAN_NEW' ); ?></a></dt>
	<?php } ?>
	</ul>
	<div class="tab-content">
		<?php if($this->showUserPosts){ ?>
		<div class="tab-pane fade active in" id="tab-1">
			<?php $this->displayUserPosts(); ?>
		</div>
		<?php }
		if ($this->showSubscriptions){ ?>
		<div class="tab-pane fade" id="tab-2">
			<?php $this->displayCategoriesSubscriptions(); ?>
			<?php $this->displaySubscriptions(); ?>
		</div>
		<?php }
		if ($this->showFavorites){ ?>
		<div class="tab-pane fade" id="tab-3">
			<?php $this->displayFavorites(); ?>
		</div>
		<?php }
		if ($this->showThankyou){ ?>
		<div class="tab-pane fade" id="tab-4">
			<?php $this->displayGotThankyou(); ?>
			<?php $this->displaySaidThankyou(); ?>
		</div>
		<?php }
		if ($this->showUnapprovedPosts){ ?>
		<div class="tab-pane fade" id="tab-5">
			<?php $this->displayUnapprovedPosts(); ?>
		</div>
		<?php }
		if ($this->showAttachments){ ?>
		<div class="tab-pane fade" id="tab-6">
			<?php $this->displayAttachments(); ?>
		</div>
		<?php }
		if ($this->showBanManager){ ?>
		<div class="tab-pane fade" id="tab-7">
			<?php $this->displayBanManager(); ?>
		</div>
		<?php }
		if ($this->showBanHistory){ ?>
		<div class="tab-pane fade" id="tab-8">
			<?php $this->displayBanHistory(); ?>
		</div>
		<?php }
		if ($this->showBanUser){ ?>
		<div class="tab-pane fade" id="tab-9">
			<?php $this->displayBanUser(); ?>
		</div>
		<?php } ?>
	</div>
</div>
