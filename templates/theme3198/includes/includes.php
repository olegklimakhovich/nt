<?php
defined('_JEXEC') or die;

JHtml::_('jquery.framework', true, null, true);
JHtml::_('bootstrap.framework');
JHtml::_('bootstrap.tooltip');
JHtml::_('bootstrap.loadCss', false, $this->direction);
JHtml::_('formbehavior.chosen', 'select');

$client = new JApplicationWebClient();

include_once 'functions.php';

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;
$template = $this->template;
$csspath = 'templates/'.$template.'/css/';
$jspath = 'templates/'.$template.'/js/';
$option = $app->input->getCmd('option', '');
$view = $app->input->getCmd('view', '');
$layout = $app->input->getCmd('layout', '');
$task = $app->input->getCmd('task', '');
$itemid = $app->input->getCmd('Itemid', '');
$sitename = $app->getCfg('sitename');
$menu = JMenu::getInstance('site');
$contentParams = $app->getParams('com_content');
$bodyClass = "body__".$contentParams->get('pageclass_sfx')." option-".$option." view-".$view." task-".$task." itemid-".$itemid;
$viewport = "";
$todesktop = '';
global $params;
$params = $this->params;

$themeLayout = $params->get('themeLayout');

if($themeLayout == 1) {
    $doc->addStyleSheet($csspath.'layout.css');
}
$doc->addStyleSheet($csspath.'template.css');
$doc->addStyleSheet($csspath.'font-awesome.css');

if(($client->platform == JApplicationWebClient::IPHONE || $client->platform == JApplicationWebClient::IPAD) && ((isset($_COOKIE['disableMobile']) && $_COOKIE['disableMobile']=='false') || !isset($_COOKIE['disableMobile']))){
    $doc->addScript($jspath.'ios-orientationchange-fix.js');
}
if($this->params->get('blackandwhite')){
    $doc->addScript($jspath.'jquery.BlackAndWhite.min.js');
    $doc->addScriptdeclaration('(function($,b){$.fn.BlackAndWhite_init=function(){var a=$(this);a.find("img").not(".slide-img").parent().BlackAndWhite({invertHoverEffect:'.$this->params->get("invertHoverEffect").',intensity:1,responsive:true,speed:{fadeIn:'.$this->params->get('fadeIn').',fadeOut:'.$this->params->get('fadeOut').'}})}})(jQuery);jQuery(window).load(function($){jQuery(".item_img a").find("img").not(".lazy").parent().BlackAndWhite_init()});');
}
$doc->addScriptdeclaration('var path = "'.$jspath.'";');
$doc->addScript($jspath.'scripts.js');

if($client->mobile){
    $bodyClass .= ' mobile';
    if($params->get('todesktop')){
        if($client->platform != 5){
            $doc->addScript($jspath.'desktop-mobile.js');
            $todesktop = "<div class=\"span12\" id=\"to-desktop\">\r\n
            <a href=\"#\">\r\n
                <span class=\"";
            if((isset($_COOKIE['disableMobile']) && $_COOKIE['disableMobile'] == 'false') || !isset($_COOKIE['disableMobile'])){
                $todesktop .= "to_desktop\">".$params->get('todesktop_text');
            }
            else{
                $todesktop .= "to_mobile\">".$params->get('tomobile_text');
            }
            $todesktop .= "</span>\r\n
            </a>\r\n
        </div>\r\n";
        }
        if((isset($_COOKIE['disableMobile']) && $_COOKIE['disableMobile'] == 'false') || !isset($_COOKIE['disableMobile'])){
            $bodyClass .= " mobile_mode";
            $viewport = "<meta id=\"viewport\" name=\"viewport\" content=\"width=device-width, initial-scale=1\">";
        }else{
            $bodyClass .= " desktop_mode";
        }
    }
}

if($option == 'com_joomgallery'){
    $doc->addStylesheet($csspath.'gallery.css');
}

if (class_exists('Komento') && Komento::loadApplication( 'com_content')) $doc->addStyleSheet($csspath.'komento.css');

if($params->get('logoFile')) $logo = $params->get('logoFile');

if($params->get('footerLogoFile')) $footerLogo = $params->get('footerLogoFile');

if($this->direction == 'rtl') $doc->addStyleSheet('media/jui/css/bootstrap-rtl.css');

$ie_warning = "";
if($client->browser == JApplicationWebClient::IE){
    if((int)$client->browserVersion<9){
        $ie_warning = "<div style=\"clear: both; text-align:center; position: relative;\"><a href=\"http://windows.microsoft.com/en-us/internet-explorer/download-ie\"><img src=\"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wgARCAAqAzQDASIAAhEBAxEB/8QAGgAAAwEBAQEAAAAAAAAAAAAAAAMEBQIGAf/EABoBAQADAQEBAAAAAAAAAAAAAAABAgQDBQb/2gAMAwEAAhADEAAAAfTEx8d7l+f3h+lD6Pm3pr565Ofz6epTP8wc6SYz3pJgpJgpJgpJgpJgpJgpJgpJgpJgpJgpJgpJgpJgpJgpJ/hSTBSTBSTBSTBSTBSTBSTBSTBSTBST/CkmCkmCkn+FJMFJMFJMFJMFJMFJMFJMFJMFJMFJ6JnrYPMnpiXmT0weZPTB5k9MHmT0weZPTYPDpOTHn66SYJSQ2ar8PWwN/P2u1572noeP4rzfqPHZt/o+eJ8OjY7x/s024JeFdn5lrlo0Y7YW0Yf21dyRCTU7yOJaL8n5S+39xeunPU5y+Da4y/hqmR1S+k7D6mu0ZHEr7sHql9B+L9mNxeXzam18xvkW3YoeYnY+5TOnPYjzPsTpOwaK2fTlc8+ml8jTMa/EKTTpx+r00ES81te3L6idVOd8vTQZm80vq/c3iY1OM7k1VISaZnfbV0VwcROsqFZqmfyO+yGXV7/M+p+l+Zy6beCNOj0Zu9Cwzi/ghqo+G356vJy6pCQ8T3KyQJRRq06Uad3vm8x6Hyx1h9+T65WaFRl1NFFbNFA0UDRQNFA0UDRQNFA0UDRQNFA0UDRQNFA0UDRQNFA0UDRQNFA0UDRQNFA0UDRQNFA0UDRQNFA0UDRQNFA0UDRQNFA0UDRQNFBpdZZ05ahlhqGWGoZYahlhqGWGpLKRZoopdooP/8QAKBAAAQUBAQEAAgECBwAAAAAAAAECAxESBBMUBSEiECQGICMxNEFg/9oACAEBAAEFArLI00dXRv8Ap5v8v9jm6PIelFlllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllmjR2uVOSziX+7VbOtuSzgcqxaNGjRo0aNGjRZo0aNGjRoss0aLNGjRZo0aLF/Ro0aNGjRo0aLNGjRo0aNGiyzRo0aNH/WizRo0aLNGjRo0aLNGjRo0MYlYQwhhDCGEMIYQwhhDCGEModv6do0aNGjuX+0shjSRvyNPzMXjzWfjl/ho5Hp6LT5WRMeOrwZG1WIxiun8mo2JqyIiO5pkajpI2RnSjY5JI2tiljjaMRvjLG1iMbGx/PhskMbZHJh8HnE6WRzfjhiSRrGwrGqRI1W5cyJnpFHG93MiSrAke2P13ORlvSOJkjIomecX0xRMmTn8F6OJ6OnVyTdfk1J+THpzsZhWsSN7IWvlViSxOqBMktRthajjkemmoni1GpJHjze5qNipRjRcyCrERu/h/Cmo1ZGsa5KaLSJJliPr1k8mOc1I3o1qtVYkTKCrG1JUbEqoxjNGhF/c/QrOv6ep6r1ye/H1Sr0R98q883XNFHCr3c0c86x/TK5ZO6Vef6JpJdH5Bf3o0aLLO7/AINn4Gb9n+I3IkVn46/Gxr1a5kr2P95PV8j3n0S59HaklfIqdMzU9H+L5XvI+hUfPP7TSTyyI6V7hkz4xs8iPSeVJG9MzUb0SsRssjYnTyudJPLKiSva1k0jGejrdNI5fol82yva6KR0S+0iyNerXej1RJ5Ucs0is9n+jZXsPeT0SV6SL0zqsHQsa+8u2Tysi0tO6JXOSR6N2uUme1t/psz2jHKxFlfj2fpJnoiSOQZIrBJpERsjmqr3OGyOaOe5y+z8terUSV6GlyyUfL6SLPJqOZWN9X7V7nCSuRVe5U9n36vxZY6SpHqyQRzUc7zc6oqXzcVEjWOaxP8ATwqRK1FYiorEPU7nWlllllnP0eZ2c3iMkdG9PzfQjZppOibj51nWedHJZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZYnXKifZKfZKfZKfZKfZKfZKfZKfZKfZKfZKfZKfZKSSukWyy/wDJ+P8A3zf16v4/jf8Az/8A/8QAKREAAQQBAgYCAQUAAAAAAAAAAAEDERICExQEITFBUVIikSAjMoGx8P/aAAgBAwEBPwGo3hb5KPvK2sIYri9gY481xXsVKlSpUqVKlSpUqVKlSpUqVKlSpUqVKlSpUqVKlSpUqVKm7TwbtPBu08G7TwbtPBu08DT6OZVgqVKDKfH+V/s4q2GdTgOeBhjOWQ5w+S5WxMWnkVJ6Gi9PU0nqdeZoOqvUbwdRyMhGX/Jm07acVF4d5e5t3vInDvcrKZsvKq1UyYdXuaD3kcZdyXkovD58qiMPJ3NB7yYsOc7eDRdwx5Gg91n/AHMVh7yKzllhVRGc568hWc8rIaDvQRp1FlRxpxV+InDuJ3Mmc7fE0Xeymi4YYOJnCmk8iGi7PURhyevI0XY6lPz4JJeQoUIHsdP9RB3h8Hf3C46aJhh3MW0wSEIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIINs16obZr1T6Ns16p9G2a9U+jbNeqfRtmvVPoxYbx54oQQf/EACsRAAEEAQIFBAEFAQAAAAAAAAABAhESAyExBBMVI1EiQWKhUgUgMnGx8P/aAAgBAgEBPwGhjxpibpuph4dHpKjsa4nHEYEast9yhQoUKFChQoUKFChQoUKFChQoUKFChQoUKFChQoUKFChQ6R8jpHy+jpHy+jpHy+jpHy+jpHy+jiP03ksvMlChQVu39J/hihWycSnqOIb/ABMbka2rkHJiVNEOzCaHavtoTiRIgyNxKyWoLyfAxMVYcgjsSexOLwWxJNUGcpESzRq4k9icP4mPltTVBHt1sgq4V9icP4jlx6VT3JxPdqWxbR/2gnJ/ESrX2QWkbaiUbVfBOPeBeWqaIY0xonqQV2Nd0G0rDkO17odvwPZjVsodpV2O1Gwq442O1OxT9/GJOJShQgxa+ka5W7G8uUd6llSCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC7vJd/ku/wAl3+S7/Jd/kVVdupBB/8QANxAAAgEDAgIIAwcEAwEAAAAAAQIRAAMSITEiQQQQEzJCUWFxI4GSIDNSkaGxwVBgctF0gvDx/9oACAEBAAY/AuryUbmsLelofr1dpGk4/PqxbW2dxQZTKHY/3DYA2aZ9eq1/lQ/5dXbxY63ykdV9fDE/0DX+tj7XRf8At+/V0O/aTUNi8fvXeP3va/Ol1nK/l1dJ/wAOrA91+GnUk9l0dNY8R51bPEA6kx5RVl+bzXFIbDIcQ/arCDPO4MvQUMHGWWOOYJ966OuvxASazTifUkZd2ukPca42GH61cc5FFCkDnrSqswUDa1cOodI0yB3rpCrnNoTJp7ryQsCBV068KBx86IYE/B7T966PIfK5qPQVDyM2aDkPPyqxiDk7RNC2twZ54xmCabswwHaxrSRM54tS3GbFHYgEsBp51Z1Ym5zG2+9XMMlXs2IYMDlVuycs3TOeQqynHlcWfaiWnFVyMV0dwGh+R86ni+8jiM86uvcyPxsIFdKBDEoVH518R1V8MtXH5RXYjPaSflVllyVXmZq0AQ2U8OQaKtkAwfOgjXnuCSNEjH9K7JstEyY/6ro9xMhnIhvaujI+Ze6kyOVJ3jcdiojasO0GWYWMxJ+VMtvKFJBJq6e0NvVdRrSZZP2nMmOdAQS5ZhvpoaXLxGAS0f8A2pg90nf0okiDgX78ml4Mk/Fl6UjOVUP+K4BA/mkx1LCZnTejOp5CYmodcSZjj/ire4VbWer0xnKFmEaecRNXi09zYe9ZEMBgWifKktlTLiZy28qkc14fVvL9KYKMnHLKOVMzZNCLpPmaygnuws+dBV5gb0QWGjRo4JPyplfI4JkYoP3RiTBb+aczliAYRp3PnTO55L3njlRI+IM8dG9JoyC3EVGsVxsgbDLv/pHWaxzhOyLfOrYR3LdirQFGpnnRLuwUXcMEC/rNHtbpw45kCNPKukMX48M007vpV1VvZns1bKBw61BuOH/EcZ/1VmekN8VyCxA4Ymkti+QO0ZDcAGulKEf4hdhmF3Aq2Lbwt4Bgfwxv1J9rop5DL9+p7J58Q6rC8y89XSW5YxPUCNxRcOczzoXO0OYED0pe0ctG21Yi4QIjYUpyPCIHpXG0/IUoW4QF9BXZZnDY6b0+TnjiflTs9x5IA0UGs4IEBRO9MGuGDuIFXZczc71HBomnftDm+5Ndp2hL44zptQAunedhQxunQk7ChbVyF3oE3NtdhUPcJWZiBTKjlct4rBHgewNW+InDu0Sbh7uOw0FYLcIWIpCrRgMV9KBRiCOdLcLnJe76VlPFMzUFyeLP3NXGFziffQUE7QxEbCmuZnM89Kt4uQE7tB89RtoNKzy4/Op7Zp9h/qhkzEKhVaRzcOSjTQaUES4QBoKRcjimwpSX2M7CnGZ4+8fOsZ0Jk1Ct7aTFKJOk0uLRB00qFPKKxy0xx2G1Kcu7qIFQGHpwjSpmSFxE/wDvWuGJ9RNaP56wJocWy4/Kmlt/SjiYnSmltxj7CoDR6xqKtgHRDIFNDasZyjWonSAPyps3OwHcDUX15ATvQOQ8+6KOpPCQKyy1iNhEe1GW39KOu4A1FcTTrlTHLVjOoFY5ekwJ6296+Iqt7isgBlETWTIpbzIqME/KtVU6RtRXBMTuIqEAUelY4rj5RQU20xGwjagQqyBAoQqiNtNuq38/tFXGVpu8tC5bOdhtm/g0GQww5193bY+ddrfaW/aiWOFpe81C3aGNldh/Z24+ag14foFeH6BXh+gV4foFeH6BXh+gV4foFeH6BXh+gV4foFeH6BXg+gVLH7fTFOq4TH2OhgaAiT/cH//EACkQAQACAgIBBAEEAwEBAAAAAAERIQAxUWFBcYGR8BAgMKGxYMHR4UD/2gAIAQEAAT8h/GrVdx9Bn/rwuXJypsxXfsxVIkJV5OR+OdmR9hT8/wCQ/wD/AP8A/wD/AP8A/wD/AP8A/wD/AP4qkIwcH8JvRZ7RHBkSKhr8FAyIh8M7/bACv7AC+v0TTr9kRSoCev7QAJUX+yAE2sYMP6gS2itT/wDIhAgBAICs6M6M6M6M6M6M6M6M6M6M6McgPKU/UD7Ph+I04gn+X3zgP1+HphQ38PI1+P0nP4cDsup85E4dyha+95C9+6Su2QHm03RGb2a8xHG0YuKHKoMUz5F/AmtYxW6p1GR2YggQHaec7fTEukayvsASU4z1CYdr/wAxKbWkW8hrGqjJCGh/3hCWg3qsYNVwVeSX8ZQI+xU2j4wqSltIB8/OXlBuEwtbOAMe/HvPWIhbMwOYNa1j4rC3eJ7k9ARM/wAZUbItDy37c5NzJ0RCDi0KIcHFY2UkhTyQfw40GTmREMWQI8iDIHKsMMDPNAjYV/AzySghyequEmxEfN5EfCcS8NvfKqNNHBtiWkKUmsl/wkzBzGE4MRMVI85RPCiqeds8yWLqeKXgQgKYJpY+ABOI/wCsk+IaFhiXrKzDbEqDyN4ZVQm0fGeOARK21WItml1BSbMhDwQhELrvGyQI4/AwyfRIhFsLQsyUN6iffIzkVTbayKwNYjHqFq3VYUlnegSOL1kuqG+964LQ2YTXEL1esnONIAbTiJ7wBHBssVwDiAWlvSpyI2GHla1MYVmTsWmr71khmJbcn+zN3xxfYUq/TL9zT0SOjAtlTsQtseIfGESYJtyZaPKNpw9MOZEdb4DG6WguxDw1ZgBBhjBgimUaCkxLyTXtldwDFF+TA5VI0AEqw8mFZ1YncSDywkT+EYd5LgrzGqYK2aYohZGoyiXCCgQlWufGS4CoGAqoTiahaUhLwuCOcSCcnKkkURqfGNlCSwL6xAQ0b3grbW88fAgZLiMpOgA8uIdsGNL6IV0lem+caZaev6hyRlhXh/Cc59M/6/E9dUdB/wCn43Y+MmdfigcJI5LKKsS53URhLUlMADoiMpyThACfQxhAOvVxMTgNuPXQwaxjwB70ZqB4Cf2ZoARACRxMTlhUnUvwNYcf4bR4h9cZ4DV5BNseuKHRwWRyxPjFlpKgsiI11lwPJQj85JgCDDJ6RGTlAJjR7duDTQpc3tWFJLCRlZfGChD4Ez8VinpcIC+ZDOIYIJ9jD9Aiiz3wXC9FD0kwkGJiQVc8ZJSKNAm4IyEF2gJjiYnGSrSFfj+/wAiKJgdAPSRGFg0WuZnGAQzUeprFASy1pgg8dY/IPIRxMTnhgxShEcYUJRgRU+2A8XwA9gZzDTQr2yQKDCHE4TAGAJKq94PRk1JlwRGEv/QKe8VnPg6p2zmmLDyJqWJcAVmYCZMvjGSIEDmMOCItI9idZOQxVPdr/GDwrSipzCmSFjKfqRiIYuWJHSJicVrpQAE8tXiUC5/pCKxncvC3fzgK1ESH9sJFg2pa247xIFpyHKu94cKiCACBmNZ41EqGT3xYndcA4AyMVxBBB07MWOdbPOE02KBk7iqxdT6utKTLUKQ0IOnGhgJ5ABEsViIIjYkXtjA+YT4Mq+f5wuQ0SpwpGHGqMgApkKMWYtACQarIEg4guI8Hea0ZGVDrUYodYWVPWPzwmfL+8KAQ1E5DC0RccZCkthZPfFJGLqPnfzjADCGRRxgCFYBh9cs9kwIxVL5tUfGLYbQx6MEokAaOMbbkyA23GIjeSr9T/A2J57O8/qGb/ZjDWSDxkDfUR/GNqFAGhwZtat/HR3nzuZ9vf+Hf/wD/AP8A/wD/AP8A/wD/AP8AIE8VYfKZ2fb1nZ9vWdn29Z2fb1nZ9vWdn29Z2fb1nZ9vWdn29Z2fb1nZ9vWdn09ZYUhBUR7fr/qyTU3f6PMIgULV/wCQf//aAAwDAQACAAMAAAAQCNnHhCCCCGCCCGKGCGCCKCCCCKCCCGCCCCKCGCCCKCCHNNNNNFCCNM+unUrezPLhUi9FYfFGz/4OIKJlhdBKZxBCKQoldBtsQwgEURNNzDMsxzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzhAAAAAQzz/8QAKREAAgIABQMEAgMBAAAAAAAAAREAITFBUWFxobHwgZHh8cHRECAwUP/aAAgBAwEBPxCXtMkAcFPntAsTzly9djCCwObXP/j/AP8A/wD/AP8Al1dfxPKfieU/E8p+J5T8Tyn4gxQ/6awPBoxM0MODf5gkpOs9RjsI1Krr9Q5HuvmsLxHtHHgDrqsuIoAMzsRjsbhaHN6qttAYGGJGemHGLma6uNsMfeFlQy6VhzcGHVjfO3EGZgSvnxQg2ACDjoeLhwpy82y2gZWbbw2pQlY7+VlQwraZICPwXlqrgVyQAPvcLegm9gDlrCEBXXW3lwouxtMc7/eM0yhgDne0vW3N4payY5wjIIzn4ymZarxdgnL2g4jM6bBVHUaIq4SNq5MeBjR7Gu3eEj1WcNIU+f1DSJIHfbSGzude0BzZ0z91LBka5YVli4XJL888EBbU5377wVD6Ho/zKTt55v8A4EDnt/IsFvSARkWV7jX0MRrYzgKsyelN9IODqLFixYsWLFixYsWLFixYsWLFixYsWLFixYsWLFixYsWLFhIWfaE+uT65Prk+uT65CKIOwAixZ//EACkRAAICAAUDAwQDAAAAAAAAAAERACExQWFx8FGRsaHB8TCB0eEQIED/2gAIAQIBAT8QgIGABJ3AKGnXrCIkVOEQHoANdL8f4AAhWj9UAR8x9IAA1fp/c5/Kc/lOfynP5Tn8oQoVZJe5/oaOCkEABZx3jQ2jRsPky+gP06d4a1HbOrxrA95QFyr9HnvEZE5Xn3FQKGT7MPXqRCsAHLrjvgoWEc3rjh2gIWdt7x2qLug1W2u+0OPnbrquOHBAkEYdRvXvDxgz4++sKMKkljrbgCvZtrZxvWG9YC/UK30dQnGMkitKgTaVWpPWAhLo/wAa7uNUE2GVfjCFgLJIsjKqxlIOreFdVPNfbGEAIOQfuUPV1hkQM48jkeubLv2gohZMwBIxNteawVEHqtRfmBAcQ8sYJbDXoFKjC018aQYOPLjEV1ZdnEFA8436RVF5zhhSl9tPGkNYXvHm0TlXmfjp9Cpbef51hYuGWkx9Cc5UI5hFixYsWLFixYsWLFixYsWLFixYsWLFixYsWLFixYsWLFiwBoH3M1vczW9zNb3M1vczW9zByMRuYsWf/8QAKBABAAIDAAMAAgIBBAMAAAAAAQARITFxQVFhgaGRsfAQIDDRYMHh/9oACAEBAAE/EOp1DWj0EQkBbYOP8RR/gNQC1wBNQVSpbwDaMSQoCI6pIj2j6r+w/f8AQHJvAD0+k9TqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdTqdRA+M4IKXk7gif8WWyN166w4/cVRM2il0j+Gdxh4eaPAfZ1Op1Op1Op1Op1OopU2PpnU6nU6nU6nUEgsroIoacJsnU6l8qaYXxOp1OoJ1bWWvU6nU6nctAN0KZ1Op1Op1Op1Op1FVCnQZZ1Op1Op1Op1OogFlWgMqx2FEaRKR9JOp1Op1Op51dpTF+rnUE3VtF48E6nU6nUUbxeSdTqdTqdTqKGnDvP8zqdTqdTqVSYQDxPinxT4p8U+KfFPinxT4p8U+KCHBC9EBQq6Sr/AJnU6nU6nUy17k7lSAAigsQMYQefMXVUV4lfg/7fYQ3K7As2nwncvT0H9J1PKKq9rAH0WFpfkAUCHzeC/wC0azWRMhWFVmKlJkQgigK+xABBW4h/7RdxYxZIXdr5vDQeSWoETTAoh+yx00SnQqJlTBj75uDbMC1YILCZUqpaegAdSAFAyn9tw8p8YtAtxh/U2sHoolpA8Ek9YnCAXlb2CqkRlVsEoCs6H9RNmUgKFrgMyvnuA0QOvFtR59cXIwAmytXf5iblXMIC9FrS61+MqcnQRisYazoIm4lgAlWwtFtAjfmGPiQMit60WLa85xF+6+S1ssrw1p1FfaxZV6gr1TLuDDtK7gWF2hrQhuywKGoXZTYFryU9rhQHKEA4vyMdDFSBEhVvujWhNtplxbda/cIG30WBYX5jA67wwpsKSrYGcREUwNABsA9Qm0tQUrV2YAv+oZraTRISNai9arcahAOi1sZ6x5MVuks1bgMWuvFUwig8ghMlYLvzqEQnrybdxEcfYjroowNoAveiJFpi1gBo+dulgHnEpqUBwUNt7xLD3aAqHHsP5imFAA6Ku8p8xzSogF8iYH5XxAqtqimAMg6N4zHWnDAsIDNFJbtIKhRFUIUAuZtd3rNYkVUUGqJnAx1KiZL6EU9pC3jeKrdYKj50ww6Nhpz69Oc6ld+GM1+EhDLTOiJFWQWM4W7GcLfmZOY0Ao2GTwBXmYGNjNDlm2uZqiFFFwVcQwrrMbGMKY54yK4LAjrSrK2mKQEuXoxXsdU1mboV5G6Kv5Ks2kFphCDerr8RBOUM8BCGGzNf1KHAOEDAHzggSCAGw3V7/HUuRLPXacwwUKZ2xgYHTS0RY0X4JTkogOGWsDNtiEwQUlsmnBq4L8WkBsULY5q3G6lXEkwrM8zjK+FrwzGoyQw6wBZmsZPUcaHKelz7dGExEQqedW61ADVWf5j5tBc7U1G6CsG4SvBkJAMmoqEJcYqNUCw4LszoggHCWTqDZaZ+4mFgCBom0vBeLr5HgALl0MFBpJktNQCCuLw8DUSkw5FAgSebde4nDDpEoF0VlXygF7NlYZsJF2j9uLc6kC/pBlOoW8AAmHyVWIH+7iKYtrBw0Eoo05HuNT0RWYe1KHaFZyXDalVqDbM2bZ/cnU6nU7ncT+SMWIKH0oNTqCr1AmjJR3j9H/RT4T7AP7l1KloSEVgYPudyla1YaRsa/EuIz1iW36FVLK8XEtgjL6Oo7sD7qL0ejGiWloIlXexUzguWn2cBaaBAfLu5lShCqtWkFceZeybEF7LRr5cay2ALFVaiuS/kogIuurH1AQ1S+4SMBUXtUgxbLbE2dxp1eNavBKeByeYTZaZu6iRtIzag8AUDGfsOrpAoQ9gT9QsHlB4oLUB6CDKMC1IoJg5RUWbuVJbSoW6X4KKxKqnGWLIuhW0KPkMGcA9CNjlp4qKs5KLkWAv5YgWijXLpsXznOZcB/lNCVg1tyU/Z52TiPV2q/EPghzDBb2VDdkQbHhBiQoXRbVzBDDdNKQ0Pw8qLj7W5Q6FXv2hASQFL9sqnlV8g0XMcO6BZoyjcYVWsWtWQK2+qjtMDTtHKhW9FEpUVbAFA0wDxcasNmjPtoOMDBm3OoUDDR5FhlpoBYUq5N+lr5L7KmnRM4A39uOrlV171QAKxXqDKYCtRlEGfuyOVk9Vp4FT7Wpcx4wMAhsX2jLNvwM1TSTb5UAJQamTHAtWvLEBrUjdhoH17cEvVrKL6haugiHpkA2ama++ICRajm1aw+HiFIHoYqsU1aqPMFGGataogl0XW54kJItIsVWn1M3kIp4hPAW1mDAl7bpTRkpTNw8VcFTE3U0NdYLFudKbisZRGNqyAqr4QCXEMXCs3RHYFShiKeJGopaxVWtd3DAUN8EgB5Mu33LDatUqIogTNZxETI0tCtQo3vf2bY4VcjDjsdOPFRbfhBVNFqtrPmssyep14tk+zxVSgOkU0w6vf5jCTKFDtDRq8OXcfGCEAfgCttGDU9CJBhpyGrvPnLKsThUZVJm2yNmXGZY2hrjm3YW5bG2Xn0q+CABWQ+vuHG7FXoCxWPe/sPZ8Cs2ahQCwQCxShtBYVDAa/cqk3ZbsDsbda8VO53C8GP9o2BFAJ7oT5MFGUBSqWrRbjUtdUQGNIi57uzIc6/L3GvyBgoWCtYMRp6gU44RWdTPE1ObdtBLBdmzJVVwryy7Jlqu3SsXG1uKjfaGsGNQuwIIhCMMXRfuWkBZ7/APcnc7nf+oFAP4cfoHuUGEfZ/oH7/kLw5kp/x+4kqyh+nbb+khAVolZ2DwZf7bZ8xyMfsXr/AODqjcNv3+V+v/DgAAAAAAAAAAAAAkIA1ytKF/n/AJX79+/fv379+/flxmPAAC3AAD8H+wXLlwGKjGcARRxeDPw/2D4fhlkDblz9ly5cuXLly5cuXLly5cuXLly5cuXLly5cuXLly5cuXLly5cuXLly5cuXLly5cuXLly5cuXLly5cuXLly5cuXLly5cuXLly5cuXLly5cuXLly5cuXP/9k=\" border=\"0\" height=\"42\" width=\"820\" alt=\"You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today.\" /></a></div>";
    }
    if((int)$client->browserVersion<10){
        $doc->addScript($jspath.'jquery.placeholder.min.js');
    }
    $doc->addScriptdeclaration('(function($){$(document).ready(function(){var c=$("#jform_profile_dob_img");if(c.length){var h=$("body").outerHeight()+26-c.offset().top;$("head").append("<style>.calendar{top:auto !important;bottom:"+h+"px !important;}</style>")}})})(jQuery);');
}

$hideByView = false;
switch ($view){
    case 'article':
    case 'login':
    case 'search':
    case 'profile':
    case 'registration':
    case 'reset':
    case 'remind':
    case 'form':
        $hideByView = true;
        break;
}

$hideByOption = false;
if($option == 'com_users' && $option == 'com_search') $hideByOption = true;

if($option == 'com_content' && $layout == 'edit') $hideByView = true;

$asideLeftWidth = "";
if($this->countModules('aside-left') && !$hideByOption && $view !== 'form') $asideLeftWidth = $params->get('asideLeftWidth');

$asideRightWidth = "";
if($this->countModules('aside-right') && !$hideByOption && $view !== 'form') $asideRightWidth = $params->get('asideRightWidth');

$mainContentWidth = 12 - $asideLeftWidth - $asideRightWidth;

global $containerClass, $rowClass;

$containerClass = 'container';
$rowClass = 'row';
if($themeLayout=='1'){
    $containerClass .= '-fluid';
    $rowClass .= '-fluid';
}

function display_position($pos){
    $html = "<!-- ".$pos." -->\n";
    $html .= "<div id=\"".$pos."\">\n";
    global $params;
    $layout = $params->get($pos.'_layout');
    if($layout == "normal"){
        global $containerClass, $rowClass;
        $html .= "<div class=\"row-container\">\n";
        $html .= "<div class=\"". $containerClass."\">\n";
        $html .= "<div class=\"".$rowClass."\">\n";
        $html .= "<jdoc:include type=\"modules\" name=\"".$pos."\" style=\"themeHtml5\" />\n";
        $html .= "</div>\n";
        $html .= "</div>\n";
        $html .= "</div>\n";
    }else{
        $html .= "<jdoc:include type=\"modules\" name=\"".$pos."\" style=\"html5nosize\" />\n";
    }
    $html .= "</div>\n";
    return $html;
}

$privacyMenuLink = $menu->getItem($params->get('privacy_link_menu'));

$privacy_link_url = JRoute::_($privacyMenuLink->link.'&Itemid='.$privacyMenuLink->id);

$termsMenuLink = $menu->getItem($params->get('terms_link_menu'));

$terms_link_url = JRoute::_($termsMenuLink->link.'&Itemid='.$termsMenuLink->id);

$back_top = '';
if($this->params->get('totop') && !$client->mobile){
    $doc->addScriptdeclaration('(function($){$(document).ready(function(){var o=$("#back-top");$(window).scroll(function(){if($(this).scrollTop()>100){o.fadeIn()}else{o.fadeOut()}});var $scrollEl=($.browser.mozilla||$.browser.msie)?$("html"):$("body");o.find("a").click(function(){$scrollEl.animate({scrollTop:0},400);return false})})})(jQuery);');
    $back_top = '<div id="back-top">
        <a href="#"><span></span>'.$this->params->get("totop_text").'</a>
    </div>';

    
} ?>