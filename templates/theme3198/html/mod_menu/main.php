<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Note. It is important to remove spaces between elements.
?>
<?php // The menu class is deprecated. Use nav instead. ?>
    <div id="shroud" class="hidden"></div>
<div class="mod-menu-main">

    <div id="hamburger-btn"></div><h1><?= JFactory::getApplication()->getMenu()->getActive()->title?></h1>
    <ul class="menu <?php echo $class_sfx;?>"<?php
    $tag = '';
    if ($params->get('tag_id') != null)
    {
        $tag = $params->get('tag_id').'';
        echo ' id="'.$tag.'"';
    }
    ?>>
        <?php
        foreach ($list as $i => &$item) :
            $class = 'item-'.$item->id;
            if ($item->id == $active_id) {
                $class .= ' current';
            }

            if (in_array($item->id, $path)) {
                $class .= ' active';
            }
            elseif ($item->type == 'alias') {
                $aliasToId = $item->params->get('aliasoptions');
                if (count($path) > 0 && $aliasToId == $path[count($path) - 1]) {
                    $class .= ' active';
                }
                elseif (in_array($aliasToId, $path)) {
                    $class .= ' alias-parent-active';
                }
            }

            if ($item->deeper) {
                $class .= ' deeper';
            }

            if ($item->parent) {
                $class .= ' parent';
            }

            if (!empty($class)) {
                $class = ' class="'.trim($class) .'"';
            }

            echo "<li".$class.">\r\n";

            // Render the menu item.
            switch ($item->type) :
                case 'separator':
                case 'url':
                case 'component':
                    require JModuleHelper::getLayoutPath('mod_menu', 'default_'.$item->type);
                    break;

                default:
                    require JModuleHelper::getLayoutPath('mod_menu', 'default_url');
                    break;
            endswitch;

            // The next item is deeper.
            if ($item->deeper) {
                echo "<div class='open-subm'></div><ul class=\"nav-child unstyled small\">\r\n";
            }
            // The next item is shallower.
            elseif ($item->shallower) {
                echo "</li>\r\n";
                echo str_repeat("</ul>\r\n</li>\r\n", $item->level_diff);
            }
            // The next item is on the same level.
            else {
                echo "</li>\r\n";
            }
        endforeach;
        ?>
    </ul>
    <script>
        var submenu_height = document.querySelector('ul.nav-child').style.height;
        document.querySelector('ul.nav-child').className += ' hidden';

        document.querySelector('.open-subm').onclick = function(){
            document.querySelector('ul.nav-child').classList.toggle('hidden');
            document.querySelector('.open-subm').classList.toggle('open');
        };

        document.querySelector('#hamburger-btn').onclick = function(){
            document.querySelector('#main_menu').classList.toggle('open');
            document.querySelector('#shroud').classList.toggle('hidden');            
        };

        document.querySelector('.request_demo').onclick = function(){
            document.querySelector('#main_menu').classList.toggle('open');
            document.querySelector('#shroud').classList.toggle('hidden');            
        };

        document.querySelector('#shroud').onclick = function(){
            document.querySelector('#main_menu').classList.toggle('open');
            document.querySelector('#shroud').classList.toggle('hidden');            
        };
    </script>
</div>

<div id="menu-line"><a href="tel:+375172856463">+7 (495) 240-84-68</a></div>
<div class="mod-menu-alt">

    <div id="menu-logo"><a href="/"></a></div>
    <ul class="menu <?php echo $class_sfx;?>"<?php
    $tag = '';
    if ($params->get('tag_id') != null)
    {
        $tag = $params->get('tag_id').'';
        echo ' id="'.$tag.'"';
    }
    ?>>
        <?php
        foreach ($list as $i => &$item) :
            $class = 'item-'.$item->id;
            if ($item->id == $active_id) {
                $class .= ' current';
            }

            if (in_array($item->id, $path)) {
                $class .= ' active';
            }
            elseif ($item->type == 'alias') {
                $aliasToId = $item->params->get('aliasoptions');
                if (count($path) > 0 && $aliasToId == $path[count($path) - 1]) {
                    $class .= ' active';
                }
                elseif (in_array($aliasToId, $path)) {
                    $class .= ' alias-parent-active';
                }
            }

            if ($item->deeper) {
                $class .= ' deeper';
            }

            if ($item->parent) {
                $class .= ' parent';
            }

            if (!empty($class)) {
                $class = ' class="'.trim($class) .'"';
            }

            echo "<li".$class.">\r\n";

            // Render the menu item.
            switch ($item->type) :
                case 'separator':
                case 'url':
                case 'component':
                    require JModuleHelper::getLayoutPath('mod_menu', 'default_'.$item->type);
                    break;

                default:
                    require JModuleHelper::getLayoutPath('mod_menu', 'default_url');
                    break;
            endswitch;

            // The next item is deeper.
            if ($item->deeper) {
                echo "<div class='open-subm'></div><ul class=\"nav-child unstyled small\">\r\n";
            }
            // The next item is shallower.
            elseif ($item->shallower) {
                echo "</li>\r\n";
                echo str_repeat("</ul>\r\n</li>\r\n", $item->level_diff);
            }
            // The next item is on the same level.
            else {
                echo "</li>\r\n";
            }
        endforeach;
        ?>
<!--      <div class="callme">+7 (495) 240-84-68</div> -->
    </ul>
    <script>
    function nt_open_drop_menu() {
        document.querySelector('.parent > .nav-child ').classList.toogle('open');
    }

    document.addEventListener("DOMContentLoaded", function(){
        // document.addEventListener('mouseenter', function(e){
        //     nt_open_drop_menu();
            console.log('dom loaded');

        document.querySelector('.mod-menu-alt .parent').addEventListener('mouseenter', function(e){
            e.target.classList.add('open');
            var ntsubm = e.target.childNodes;
            // console.log(ntsubm.indexOf('ul.nav-child.unstyled.small'));
            ntsubm[4].classList.add('open');  
        })

        document.querySelector('.mod-menu-alt .parent').addEventListener('mouseleave', function(e){
            e.target.classList.remove('open');
            var ntsubm = e.target.childNodes;
            // console.log(ntsubm.indexOf('ul.nav-child.unstyled.small'));
            ntsubm[4].classList.remove('open');  
        })

    });

    </script>

</div>
