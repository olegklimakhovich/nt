<?php defined('_JEXEC') or die;
include_once('includes/includes.php');?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>"
      lang="<?php echo $this->language; ?>">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-P8JW6B');</script>
    <!-- End Google Tag Manager -->
<!--  <style> 
#header > div:nth-child(2) {
    height: 0!important;
}

</style> -->
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <?php echo $viewport; ?>
    <jdoc:include type="head"/>
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300,500,700,400italic,300italic' rel='stylesheet'
          type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Righteous' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
    <link href='/templates/theme3198/fonts/intro/intro.css' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/templates/theme3198/css/adaptive.css">
</head>
<body class="<?php echo $bodyClass; ?>">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P8JW6B"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php echo $ie_warning; ?>
<!-- Body -->
<div id="wrapper">
    <div class="wrapper-inner">
        <a id="fake" href='#'></a>
        <div id="header">
            <!-- Top -->
            <div id="top">
                <div class="w-container">
                    <!-- Logo -->
                    <div id="logo" class="span<?php echo $this->params->get('logoBlockWidth'); ?>">
                        <a href="<?php echo JURI::base(); ?>">
                            <?php if (isset($logo)) { ?>
                                <img src="/templates/theme3198/images/wLogo.svg" alt="<?php echo $sitename; ?>">
                                <!--<img src="<?php echo $logo; ?>" alt="<?php echo $sitename; ?>">                                        -->
                            <?php } ?>
                            <h1><?php echo wrap_chars_with_span($sitename); ?></h1>
                        </a>
                    </div>
                    <jdoc:include type="modules" name="top" style="html5nosize"/>
                    <div class="clearfix"></div>
                </div>
            </div>
            <?php if ($this->countModules('header') && !$hideByView) { ?>
                <jdoc:include type="modules" name="header" style="html5nosize"/>
            <?php } ?>

            <?php if ($this->countModules('breadcrumbs')) { ?>
                <div class="row-container">
                    <div class="<?php echo $containerClass; ?>">
                        <!-- Breadcrumbs -->
                        <div id="breadcrumbs" class="<?php echo $rowClass; ?>">
                            <jdoc:include type="modules" name="breadcrumbs" style="themeHtml5"/>
                        </div>
                    </div>
                </div>
            <?php }
            ?>
        </div>
        <?php if (!$hideByView) {

            if ($this->countModules('navigation')) {
                echo display_position('navigation');
            }
            if ($this->countModules('feature')) {
                echo display_position('feature');
            }
            if ($this->countModules('showcase')) {
                echo display_position('showcase');
            }
            if ($this->countModules('maintop')) {
                echo display_position('maintop');
            }
        } ?>
        <!-- Main Content row -->
        <div id="content">
            <?php
            if ($this->countModules('map') && !$hideByView) { ?>
                <!-- Map -->
                <div id="map">
                    <jdoc:include type="modules" name="map" style="html5nosizeMap"/>
                </div>
            <?php }
            $layout = $params->get('content_layout');
            if ($layout == "normal"){ ?>
            <div class="row-container">
                <div class="<?php echo $containerClass; ?>">
                    <?php } ?>
                    <div class="content-inner <?php echo $rowClass; ?>">
                        <?php if ($this->countModules('aside-left') && !$hideByOption && $view !== 'form') { ?>
                            <!-- Left sidebar -->
                            <div id="aside-left" class="span<?php echo $asideLeftWidth; ?>">
                                <aside role="complementary">
                                    <jdoc:include type="modules" name="aside-left" style="html5nosize"/>
                                </aside>
                            </div>
                        <?php } ?>
                        <div id="component" class="span<?php echo $mainContentWidth; ?>">
                            <main role="main">
                                <?php if ($this->countModules('content-top') && !$hideByView) { ?>
                                    <!-- Content-top -->
                                    <div id="content-top" class="<?php echo $rowClass; ?>">
                                        <jdoc:include type="modules" name="content-top" style="themeHtml5"/>
                                    </div>
                                <?php } ?>
                                <jdoc:include type="message"/>
                                <jdoc:include type="component"/>
                                <?php if ($this->countModules('content-bottom') && !$hideByOption && $view !== 'form') { ?>
                                    <!-- Content-bottom -->
                                    <div id="content-bottom" class="<?php echo $rowClass; ?>">
                                        <jdoc:include type="modules" name="content-bottom" style="themeHtml5"/>
                                    </div>
                                <?php } ?>
                            </main>
                        </div>
                        <?php if ($this->countModules('aside-right') && !$hideByOption && $view !== 'form') { ?>
                            <!-- Right sidebar -->
                            <div id="aside-right" class="span<?php echo $asideRightWidth; ?>">
                                <aside role="complementary">
                                    <jdoc:include type="modules" name="aside-right" style="html5nosize"/>
                                </aside>
                            </div>
                        <?php } ?>
                    </div>
                    <?php if ($layout == "normal"){ ?>
                </div>
            </div>
        <?php } ?>
        </div>
        <?php if (!$hideByView) {
            if ($this->countModules('video')) {
                echo display_position('video');
            }
            if ($this->countModules('mainbottom')) {
                echo display_position('mainbottom');
            }
            if ($this->countModules('mainbottom-2')) {
                echo display_position('mainbottom-2');
            }
            if ($this->countModules('mainbottom-3')) {
                echo display_position('mainbottom-3');
            }
            if ($this->countModules('mainbottom-4')) {
                echo display_position('mainbottom-4');
            }
            if ($this->countModules('mainbottom-5')) {
                echo display_position('mainbottom-5');
            }
            if ($this->countModules('footer')) {
                echo display_position('footer');
            }
        } ?>
        <div id="push"></div>
    </div>
</div>
<div id="footer-wrapper">
    <div class="footer-wrapper-inner">
        <!-- Copyright -->
        <div id="copyright" role="contentinfo">
            <jdoc:include type="modules" name="copyright2" style="html5nosize"/>
            <div class="row-container">
                <div class="<?php echo $containerClass; ?>">
                    <div class="<?php echo $rowClass; ?>">
                        <jdoc:include type="modules" name="copyright" style="themeHtml5"/>
                        <?php //echo $todesktop; ?>
                        <!-- {%FOOTER_LINK} -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
if ($this->countModules('bottom')) {
    echo display_position('bottom');
}

?>
<div class="footer-contacts">
    <div id="buttom_logo" class="span<?php echo $this->params->get('logoBlockWidth'); ?>">
        <a href="<?php echo JURI::base(); ?>">
            <?php if (isset($logo)) { ?>
                <img src="/templates/theme3198/images/wLogo.svg" alt="<?php echo $sitename; ?>">
                <!--<img src="<?php echo $logo; ?>" alt="<?php echo $sitename; ?>">                                        -->
            <?php } ?>
        </a>
    </div>
    <jdoc:include type="modules" name="footer-contacts" style="html5nosize"/>
</div>
<?php echo $back_top;
if ($this->countModules('modal')) {
    $doc->addScriptdeclaration('(function($){$(document).ready(function(){var o=$(\'a[href="#modal"]\');o.click(function(e){$("#modal").modal({show:true,backdrop:false});e.preventDefault()});$("#modal button.modalClose").click(function(e){$("#modal").modal("hide");e.preventDefault()});$(".modal-hide").click(function(e){$("#modal").modal("hide")})})})(jQuery);'); ?>
    <div id="modal" class="modal hide fade loginPopup">
        <div class="modal-hide"></div>
        <div class="modal_wrapper">
            <button type="button" class="close modalClose">×</button>
            <jdoc:include type="modules" name="modal" style="modal"/>
        </div>
    </div>
<?php }
if ($this->countModules('fixed-sidebar-left')) { ?>
    <jdoc:include type="modules" name="fixed-sidebar-left" style="none"/>
<?php }
if ($this->countModules('fixed-sidebar-right')) { ?>
    <div id="fixed-sidebar-right">
        <jdoc:include type="modules" name="fixed-sidebar-right" style="sidebar"/>
    </div>
<?php } ?>
<jdoc:include type="modules" name="debug" style="none"/>
<div class="custom rating" style="display:none;          ">
    <div itemtype="http://data-vocabulary.org/Review-aggregate" itemscope=""><span itemprop="itemreviewed"
                                                                                   style="font-size: 8pt;">Наш рейтинг: 5.0&nbsp;</span><span
            style="font-size: 8pt;">(голосов:<span itemprop="votes"> &nbsp;55</span>)&nbsp;<img src="/images/stars.png"
                                                                                                alt="stars" width="9"
                                                                                                height="8"><span>&nbsp;</span><img
                src="/images/stars.png" alt="stars" width="9" height="8"><span>&nbsp;</span><img src="/images/stars.png"
                                                                                                 alt="stars" width="9"
                                                                                                 height="8"><span>&nbsp;</span><img
                src="/images/stars.png" alt="stars" width="9" height="8"><span>&nbsp;</span><img src="/images/stars.png"
                                                                                                 alt="stars" width="9"
                                                                                                 height="8">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</span>
        <span itemtype="http://data-vocabulary.org/Rating" itemscope="" itemprop="rating">
            <meta content="5.0" itemprop="value">
            <meta content="5" itemprop="best">
            </span>
    </div>
</div>
</body>
</html>